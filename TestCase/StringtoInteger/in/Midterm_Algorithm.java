/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package midterm_algorithm;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Pinkz7_
 */
public class Midterm_Algorithm {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        String test = kb.next();
        System.out.print(StringtoInt(test));
    }

    private static int StringtoInt(String test) {
        int sum=0;
        int point=1;
        for(int i=test.length()-1; i>=0;i--){
            if(test.charAt(i)=='1'){
                sum=sum+1*point;
            }else if(test.charAt(i)=='2'){
                sum=sum+2*point;
            }else if(test.charAt(i)=='3'){
                sum=sum+3*point;
            }else if(test.charAt(i)=='4'){
                sum=sum+4*point;
            }else if(test.charAt(i)=='5'){
                sum=sum+5*point;
            }else if(test.charAt(i)=='6'){
                sum=sum+6*point;
            }else if(test.charAt(i)=='7'){
                sum=sum+7*point;
            }else if(test.charAt(i)=='8'){
                sum=sum+8*point;
            }else if(test.charAt(i)=='9'){
                sum=sum+9*point;
            }else if (i == 0 && test.charAt(i) == '-') { 
                sum *= -1;
            }
            point=point*10;
        }
        return sum;
    }
    
}
