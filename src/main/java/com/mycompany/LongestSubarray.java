/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Pinkz7_
 */
public class LongestSubarray {

    
    public static void main(String[] args) {
        Scanner kb = new Scanner(System.in);
        ArrayList<Integer> a = new ArrayList<>();//create arraylist a
        while (kb.hasNext()) {//loop input until end of last input
            a.add(Integer.parseInt(kb.next()));
            System.out.print(sort(a));//print sort(logestSubarray)
        }
    }
        
    private static String sort(ArrayList<Integer> m) {
        int count = 1; //นับจำนวนว่าเรียงกันของข้อมูลได้กี่จำนวน
        int lenght = 1; //ผลลัพธ์
        int sort = 0; //ตัวที่เรียกกันของชุดข้อมูลที่ยาวที่สุด
        String x = "";
        for (int i = 1; i < m.size(); i++) {
            if (m.get(i) >= m.get(i - 1)) {//ตรวจสอบว่าตัวปัจจุบันกับตัวก่อนหน้าว่าตัวปัจจุบันมากกว่าหรือไม่
                count++;
            } else if (count > lenght) {//เช็คว่าจำนวนเรียงกันของข้อมูลมากกว่าผลลัพธ์
                lenght = count;
                sort = i - lenght;
                count = 1;
            } else if (count < lenght && i != m.size() - 1 && m.get(i) < m.get(i - 1)) {//จำนวนเรียงกันของข้อมูลน้อยกว่าผลลัพธ์ และตัวปัจจุบันไม่เท่ากับตัวสุดท้ายของ m และตัวปัจจุบันน้อยกว่าตัวก่อนหน้า
                count = 1;
            }
        }
        if (lenght < count) {//ผลลัพธ์น้อยกว่าจำนวนที่เรียงกันของข้อมูล
            lenght = count;
            sort = m.size() - lenght;
        }
        for (int i = sort; i < lenght + sort; i++) {
            x = x + m.get(i) + " ";
        }
        return x; //ส่งข้อมูลกลับ
    }
}
